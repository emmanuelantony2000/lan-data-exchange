import os
from multiprocessing import Pool

# temp = ('first.py')
programs = ('mainsender.py', 'mainreceiver.py')


def runProcess(program):
    os.system('python3 {}'.format(program))


pool = Pool(processes=3)
# pool.map(runProcess, temp)
pool.map(runProcess, programs)
