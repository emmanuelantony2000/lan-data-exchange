import os
import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
selfadd = s.getsockname()[0]
s.close()
# selfadd = socket.gethostbyname(socket.gethostname())
port = 13000
buf = 1024
addr = (selfadd, port)

receive = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
receive.bind(addr)

print('Waiting for message..')

while True:
    (data, addr) = receive.recvfrom(buf)
    print('Message: ', data.decode())
    if data.decode() == 'exit':
        break

receive.close()
os._exit()
