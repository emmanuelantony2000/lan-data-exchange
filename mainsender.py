import os
import socket
import networkSearcher
import pandas as pd
import variables


data = pd.read_pickle('data.pickle')


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
selfadd = s.getsockname()[0]
s.close()
iplist = networkSearcher.map_network()
del iplist[iplist.index(selfadd)]
print(iplist)

port = 13000
send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    url = str(input('Enter url: '))
    data = data.append({variables.column[0]: url}, ignore_index=True)
    data.to_pickle('data.pickle')
    for ip in iplist:
        send.sendto(url.encode('utf-8'), (ip, port))
    if url == 'exit':
        break

send.close()
os._exit(0)
