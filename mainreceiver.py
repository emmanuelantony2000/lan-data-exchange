import os
import socket
import pandas as pd
import variables


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
selfadd = s.getsockname()[0]
s.close()
# selfadd = socket.gethostbyname(socket.gethostname())
port = 13000
buf = 16384
addr = (selfadd, port)

receive = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
receive.bind(addr)

# print('Waiting for message..')

while True:
    (url, addr) = receive.recvfrom(buf)
    url = url.decode()
    data = pd.read_pickle('data.pickle')
    if not data[data[variables.column[0]] == url].empty:
        print('Yes')
    print('Message: ', url)
    if url == 'exit':
        break

receive.close()
os._exit(0)
