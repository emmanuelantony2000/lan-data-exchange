import os
import socket
import networkSearcher


s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.connect(("8.8.8.8", 80))
selfadd = s.getsockname()[0]
s.close()
iplist = networkSearcher.map_network()
# del iplist[iplist.index(selfadd)]
print(iplist)

port = 13000
send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    data = str(input('Enter the message to send: '))
    for ip in iplist:
        send.sendto(data.encode('utf-8'), (ip, port))
    if data == 'exit':
        break

send.close()
os._exit(0)
