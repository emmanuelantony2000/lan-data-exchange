import os
import socket
host = '192.168.43.37'  # Set to IP address of target computer
port = 13000
addr = (host, port)
send = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
while True:
    data = str(input('Enter message to send or type \'exit\': '))
    send.sendto(data.encode('utf-8'), addr)
    if data == 'exit':
        break
send.close()
os._exit(0)
